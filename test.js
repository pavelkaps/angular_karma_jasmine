describe('UserService Service', () => {
    //Fake objects
    const Users = [
        {
            name: 'John',
            country: 'RU'
        },
        {
            name: 'Jilly',
            country: 'AU'
        }
    ];

    const backendUrl = 'http://localhost/users';

    let BankService;
    let httpBackend;
    let http;

    beforeEach(angular.mock.module('calculator'));

    beforeEach(inject(($httpBackend, $http, _BankService_) => {
        httpBackend = $httpBackend;
        http = $http;
        BankService = _BankService_;
    }));

    it('Service should exist', () => {
        expect(BankService).toBeDefined();
    });

    describe('.getUsersByName', () => {
        it('getUsersByName should exist', () => {
            expect(BankService.getUsersByName).toBeDefined();
        });

        it('when find Jilly should return right users', findName('Jilly'));

        it('when find Jack should return empty result', findName('Jack'));

        it('should catch error when response code 400', (done) => {
            const name = 'Kate';
            BankService
                .getUsersByName(name, backendUrl)
                .catch(done);

            httpBackend
                .when('GET', `${backendUrl}?name=${name}`)
                .respond(400);

            httpBackend.flush();
        });
    });

    describe('.getUsers', () => {
        it('should exist', () => {
            expect(BankService.getUsers).toBeDefined();
        });

        it('should return some users', () => {
            let users;
            BankService
                .getUsers(backendUrl)
                .then((data) => {
                    users = data.users;
                });

            httpBackend
                .when('GET', backendUrl)
                .respond(200, { users: Users });

            httpBackend.flush();

            expect(users).toEqual(Users);
        });

        it('should catch error when response code 400', (done) => {
            BankService
                .getUsers(backendUrl)
                .catch(done);

            httpBackend
                .when('GET', backendUrl)
                .respond(400);

            httpBackend.flush();
        });
    });

    function findName(name) {
        return () => {
            let users;

            BankService
                .getUsersByName(name, backendUrl)
                .then((data) => {
                    users = data.users;
                });

            const rightUsers = Users.filter(user => user.name === name);
            httpBackend
                .when('GET', `${backendUrl}?name=${name}`)
                .respond(200, { users: rightUsers });

            httpBackend.flush();

            expect(users).toEqual(rightUsers);
        }
    }
});
