describe('Calculator Controller', () => {
    let CalculatorCtrl;
    let MoneyService;
    let BankService;

    let httpBackend;

    beforeEach(angular.mock.module('calculator'));

    beforeEach((done) => inject(($httpBackend, $controller, _MoneyService_, _BankService_) => {
        MoneyService = _MoneyService_;
        BankService = _BankService_;
        httpBackend = $httpBackend;
        //spyOn(BankService, 'getRatio').and.callFake(getRatio);
        //spyOn(MoneyService, 'create').and.callFake(createCurrency);

        CalculatorCtrl = $controller('CalculatorCtrl', { MoneyService, BankService });
        done();
    }));


    it('Ctrl should exist', () => {
        expect(CalculatorCtrl).toBeDefined();
    });

    it('MoneyService should exist', () => {
        expect(MoneyService).toBeDefined();
    });

    it('BankService should exist', () => {
        expect(BankService).toBeDefined();
    });

    describe('Calculator', () => {
        it('Should convert 1000 USD to 27 000 UAH', (done) => {
            const backendUrl = 'https://privat24.ua';

            const usd = MoneyService.create('USD', 1000);
            const uah = MoneyService.create('UAH');

            let usdConvert = null;
            CalculatorCtrl
                .convertCurrency(usd, uah)
                .then(convertValue => {
                    usdConvert = convertValue;
                    done();
                });

            httpBackend
                .when('GET', `${backendUrl}/?firstCurr=${usd.getType()}&secondCurr=${uah.getType()}`)
                .respond(200, { ratio: 27.0 });

            httpBackend.flush();

            expect(usdConvert).toEqual(27000);
        });

        it('Should convert 27 000 UAH to 1000 USD', (done) => {
            const backendUrl = 'https://privat24.ua';

            const usd = MoneyService.create('USD');
            const uah = MoneyService.create('UAH', 27000);

            let usdConvert = null;
            CalculatorCtrl
                .convertCurrency(uah, usd)
                .then(convertValue => {
                    usdConvert = convertValue;
                    done();
                });

            httpBackend
                .when('GET', `${backendUrl}/?firstCurr=${uah.getType()}&secondCurr=${usd.getType()}`)
                .respond(200, { ratio: 1 / 27.0 });

            httpBackend.flush();

            expect(usdConvert).toEqual(1000);
        });
    })
});

function createCurrency() {

}