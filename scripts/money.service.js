(function Service() {
    /* @ngInject */
    function Service(Dollar, Hryvna) {
        class Money {
            create(type, amount) {
                switch (type) {
                    case new Dollar().getType() : {
                        return new Dollar(amount);
                    }
                    case new Hryvna().getType() : {
                        return new Hryvna(amount);
                    }
                }
            }
        }

        return new Money();
    }

    angular
        .module('calculator')
        .service('MoneyService', Service);
}());