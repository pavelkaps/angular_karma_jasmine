(function Service() {
    /* @ngInject */
    function Service() {
        class Currency {
            constructor(type, amount = 0) {
                this.type = type;
                this.amount = amount;
            }

            getType() {
                return this.type;
            }

            getAmount() {
                return this.amount;
            }
        }

        return Currency;
    }

    angular
        .module('calculator')
        .service('Currency', Service);
}());
