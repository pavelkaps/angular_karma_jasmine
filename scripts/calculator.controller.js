(function ContentPage() {
  /* @ngInject */
  function Controller(MoneyService, BankService) {
      this.currencyUSD = MoneyService.create('USD', 1000);
      this.currencyUAH = MoneyService.create('UAH');

      this.convertCurrency = (fromCurr = this.currencyUSD, toCurr = this.currencyUAH) => {
        return BankService
          .getRatio(fromCurr, toCurr)
          .then((ratio) => {
            return fromCurr.getAmount() * ratio;
          })
      }
  }

  angular
    .module('calculator')
    .controller('CalculatorCtrl', Controller);
}());
