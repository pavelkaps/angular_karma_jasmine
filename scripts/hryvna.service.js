(function Service() {
    /* @ngInject */
    function Service(Currency) {
        class Hryvna extends Currency {
            constructor(amount) {
                super('UAH', amount);
            }
        }

        return Hryvna;
    }

    angular
        .module('calculator')
        .service('Hryvna', Service);
}());
