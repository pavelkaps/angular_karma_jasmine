(function Service() {
  /* @ngInject */
  function Service($http) {
    const bankRoute = 'https://privat24.ua';

    this.getRatio = (firstCurr, secondCurr, routePath = bankRoute) => {
      const path = `${routePath}/?firstCurr=${firstCurr.getType()}&secondCurr=${secondCurr.getType()}`;
      return $http.get(path).then(({ data }) => {
        return data.ratio;
      });
    };
  }

  angular
    .module('calculator')
    .service('BankService', Service);
}());
